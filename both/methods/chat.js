import {Messages} from './../../imports/api/messages.js'
import {Rooms} from './../../imports/api/rooms.js'
Meteor.methods({
  sendMessage: function (params) {
    Messages.insert({
      messageText: params.messageText,
      createdAt: new Date(),
      username: params.username,
      room_name:params.room_name
    });
  },
  insertRoom(name){
    var room=Rooms.find({'name':name}).fetch()
    if (typeof(room)!=="undefined" && room!==null && room.length<1){
        Rooms.insert({name:name})
    }

  }
});
