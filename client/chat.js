import { ReactiveDict } from 'meteor/reactive-dict';
import { Cookies } from 'meteor/ostrio:cookies';
import '../imports/startup/client/urls.js';
const cookies = new Cookies();
import { Messages } from './../imports/api/messages.js';
import { Rooms } from './../imports/api/rooms.js';


/* scrolling code */
if (Meteor.isClient) {
    // This code only runs on the client
    Meteor.subscribe("messages");
    Meteor.subscribe("rooms");
    Meteor.startup(function () {
        //$('#my-popup-link').popup('#my-popup-box')
        cookie_username=cookies.get('username');
        if(cookie_username==null){
            Session.set('activeModal', 'joinModal');
        }
    })
    Template.body.events({
      'click span.closemodal': function(event, template) {
        Session.delete('activeModal');
      }
    });

    /*    __  _______      __      __
       /  |/  / __ \____/ /___ _/ /
      / /|_/ / / / / __  / __ `/ /
     / /  / / /_/ / /_/ / /_/ / /
    /_/  /_/\____/\__,_/\__,_/_/
*/
    Template.modal.helpers({
      activeModal: function() {
        return Session.get('activeModal');
      }
    });
    Template.joinModal.events({
      'click button.save_username': function(event, template) {
        cookies.set('username', template.find('.username').value)
        username=template.find('.username').value
        if (username!==""){
            room_template_instance.username.set(template.find('.username').value);
            cookie_username=template.find('.username').value
            //this.username=template.find('.username').value
            Session.delete('activeModal', 'joinModal');
        }
        else{
            alert('Username connot be empty')
        }

      }
    });
    Template.joinRoomModal.events({
      'click button.join_room': function(event, template) {
        var room_name='/room/'+template.find('.selected-room-name').value
        Router.go(room_name)
        Session.delete('activeModal');
      }
    });

/*    ____
   / __ \____  ____  ____ ___
  / /_/ / __ \/ __ \/ __ `__ \
 / _, _/ /_/ / /_/ / / / / / /
/_/ |_|\____/\____/_/ /_/ /_/
*/
    Template.Room.events({
      'click button.modal': function(event, template) {
        var name = template.$(event.target).data('modal-template');
        Session.set('activeModal', name);
      },

      'click .talk-button , submit .new-message ': function(event, template) {
        var text=$('#text')
        var q_room_name =this.roomName
        call_params={
            messageText:text.val(),
            room_name:q_room_name,
            username:cookie_username
        }
        Meteor.call("sendMessage", call_params);
        text.val('');
        event.preventDefault();
        $("html, body").animate({
            scrollTop: $('.chat .message').last().eq(0).position().top -100
        }, "slow");
      }
    });

    Template.Room.onCreated(function(){
        //this.state = new ReactiveDict();
        this.messages = new ReactiveDict();
        this.rooms = new ReactiveDict();
        Meteor.call("insertRoom", this.data.roomName);

        $("html, body").animate({
            scrollTop: window.innerHeight*100
        }, "slow");
        this.previous_message_length=parseInt(this.messages.length)

        this.username = new ReactiveVar(cookies.get('username'));
        room_template_instance=Template.instance()//Это передалть

      });

    Template.Room.helpers({
       username: function(){
          return Template.instance().username.get();
       },
        messages() {
           return Messages.find({room_name:qroomName},{sort: {createdAt: 1}})

         },
        rooms() {
           return Rooms.find()
         },
    });






  /*account config*/
}
