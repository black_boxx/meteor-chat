import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import {Rooms} from './rooms.js'

export const Messages = new Mongo.Collection('messages');

Messages.allow({
    insert:function(){
        return true;
    },
    update:function(){
        return false;
    },
    remove:function(){
        return false;
    }
});

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish('messages', function messagesPublication() {
    return Messages.find()//Messages.find({}, {sort: {createdAt: -1}});
  });
}

Meteor.methods({
  'messages.insert'(text,username,room) {

    Messages.insert({
      text,
      createdAt: new Date(),
      //owner: Meteor.userId(),
      username: username,
      room:room
    });

  },
});
