import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export const Rooms = new Mongo.Collection('rooms');

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish('rooms', function roomsPublication() {
    return Rooms.find({})//Messages.find({}, {sort: {createdAt: -1}});
  });
}

Meteor.methods({
  'rooms.insert'(name) {

    Rooms.findAndModify({
      query: { name: name },
      update: {
        $setOnInsert: { name: name }
      },
      new: true,   // return new doc if one is upserted
      upsert: true // insert document if it does not exist
    })

  },
});

