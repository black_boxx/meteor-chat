import { Messages } from './../../api/messages.js';



Router.route('/', function() {
  this.redirect('/room/welcome');
});

Router.route('/room/:rn', {
    template: 'Room',
    data: function(){
        qroomName = this.params.rn;
        result={
            roomName:qroomName,
        }
        return result
    }
});